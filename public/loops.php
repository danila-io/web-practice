<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Loops #1, #2 and #3</title>
</head>

<body>
    <?php
        $arr = [
            ['href'=>'1.html', 'text'=>'ссылка 1'],
            ['href'=>'2.html', 'text'=>'ссылка 2'],
            ['href'=>'3.html', 'text'=>'ссылка 3'],
        ];
    ?>

    <ul>
        <?php
            foreach ($arr as $elem) {
                echo "<li><a href=\"{$elem['href']}\">{$elem['text']}</a></li>";
            }
        ?>
    </ul>

    <br><br>

    <?php
        $arr1= [
            ['name'=>'Коля', 'age'=>30, 'salary'=>500],
            ['name'=>'Вася', 'age'=>31, 'salary'=>600],
            ['name'=>'Петя', 'age'=>32, 'salary'=>700],
        ];
    ?>

    <table>
        <tr>
            <?php
                foreach ($arr1 as $elem1) {
                    echo "<tr><td>{$elem1['name']}</td><td>{$elem1['age']}</td><td>{$elem1['salary']}</td></tr>";
                }
            ?>
        </tr>
    </table>
</body>

</html>