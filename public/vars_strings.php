<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Variables #1 and #2</title>
</head>

<body>
    <?php
        $name = "Иван";
        echo "Привет " . $name . "!";
    ?>
    <br><br>
    <?php
        $age = array("name"=>"Иван", "age"=>"30");
        echo "Привет, " . $age['name'] . "!  Тебе " . $age["age"] . " лет!";
    ?>
</body>

</html>