<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>File parsing</title>
</head>
<body>
    <?php
        ini_set('display_errors', 1);
        error_reporting(E_ALL);

        $row = 1;
        if (($handle = fopen("items.csv", "r")) !== FALSE) {
   
            echo '<table border="1">';
           
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);

                if ($row == 1) {
                    echo '<thead><tr>';
                } else {
                    echo '<tr>';
                }
               
                for ($c=0; $c < $num; $c++) {
                    if (empty($data[$c])) {
                       $value = "&nbsp;";
                    } else {
                       $value = $data[$c];
                    }
                    if ($row == 1) {
                        echo '<th>' . $value . '</th>';
                    } else {
                        outputData($value);
                    }
                }
               
                if ($row == 1) {
                    echo '</tr></thead><tbody>';
                } else {
                    echo '</tr>';
                }

                $row++;
            }
           
            echo '</tbody></table>';
            fclose($handle);
        }

        function outputData($value) {
            if (strpos($value, '.html') !== false) {
                echo "<td><a href=\"/IO/descriptions/". $value . "\">Description</a></td>";
            } else {
                echo '<td>'.$value.'</td>';
            }
        }
    ?>
</body>
</html>