<?php
    ini_set('display_errors', 1);
    error_reporting(E_ALL);

    echo 'Число var1: ' . $_GET['var1'] . '<br>';
    echo 'Число var1 во второй степени: ' . pow($_GET['var1'], 2) . '<br>';
    echo 'Сумма двух чисел (var1 + var2): ' . ($_GET['var1'] + $_GET['var2']) . '<br>';

    echo 'Результат оператора ветвления: ';
    if ($_GET['var1'] == 1) {
        echo 'Привет';
    } else if ($_GET['var1'] == 2) {
        echo 'Пока';
    } else {
        echo 'Вы ввели число отличное от 1 и 2';
    }
?>

