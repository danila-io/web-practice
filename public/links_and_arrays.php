<?php
    ini_set('display_errors', 1);
    error_reporting(E_ALL);

    $arr = ['a', 'b', 'c', 'd', 'e', 'f', 'g'];

    if (isset($_GET['num']) and isset($arr[$_GET['num']])) {
        echo $arr[$_GET['num']];
    }

    echo "<br>";

    foreach ($arr as $key => $element) {
        echo "<a href=\"?num=$key\">link $element</a> ";
    }
?>