function changeText() {
    var text = document.getElementById("text");
    text.innerHTML= "Hello world!";
    text.setAttribute("style", "color: red;");

    var button = document.getElementById("button");
    button.setAttribute("disabled", "disabled");
    button.innerHTML = "Button deactivated";
}