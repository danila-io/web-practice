function changeText() {
    var textElements = document.getElementsByTagName("p");

    for (let index = 0; index < textElements.length; index++) {
        textElements[index].innerHTML = (index + 1) + ". " + textElements[index].innerHTML;
    }
}