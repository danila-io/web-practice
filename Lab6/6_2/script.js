function displayLinks() {
    var links = document.getElementsByTagName("a");

    for (let index = 0; index < links.length; index++) {
        links[index].innerHTML = links[index].innerHTML + " " + 
        links[index].getAttribute("href");
    }
}