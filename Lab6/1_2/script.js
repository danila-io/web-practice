function changeText() {
    var text = document.querySelector('#text');
    text.innerHTML = "Абзац превратился в h3!";

    var newText = document.createElement('h3');
    newText.innerHTML = text.innerHTML;

    text.parentNode.replaceChild(newText, text);
}