function changeText() {
    var text = document.querySelector('#text');

    var newText = document.createElement('h3');
    newText.innerHTML = text.innerHTML;

    text.parentNode.replaceChild(newText, text);
}