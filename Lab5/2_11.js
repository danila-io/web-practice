console.log("Дан массив с числами. С помощью цикла найдите сумму квадратов элементов этого массива.");

console.log("Решение:");

var array = [11, 20, 10, 5, 3, 7, 88, 3, 4];
var sum = 0;

console.log("Изначальный массив: " + array);

array.forEach(e => {
    sum += Math.sqrt(e);
});

console.log("Сумма чисел в массиве: " + sum);