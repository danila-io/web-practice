console.log("Заполните массив числами от 1 до 10 с помощью цикла.");

console.log("Решение:");

var array = [];

for (let index = 0; index < 10; index++) {
    array.push(index + 1);
}

console.log("Ответ: " + array);