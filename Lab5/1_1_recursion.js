console.log("Дан массив с числами. Выведите последовательно его элементы используя рекурсию и не используя цикл.\n");

var arr = [1, 4, 6, 7, 4, 3, 5, 10, 4, 5];

console.log("Решение:")
printSingleElement(arr);

function printSingleElement(arr) {
    console.log(arr.pop());

    if (arr.length != 0) {
        printSingleElement(arr);
    }
}