console.log("Заполните массив 10-ю случайными числами от 1 до 10 с помощью цикла.");

console.log("Решение:");

var array = [];

for (let index = 0; index < 10; index++) {
    array.push((Math.random() * (10 - 1) + 1).toPrecision(1));
}

console.log("Ответ: " + array);