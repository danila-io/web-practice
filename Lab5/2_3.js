console.log("Выведите с помощью цикла столбец четных чисел от 1 до 100.");

console.log("Решение:");

for (let index = 2; index < 101; index += 2) {
    console.log(index);
}