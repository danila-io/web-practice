console.log("Дано число. Сложите его цифры. Если сумма получилась менее 9-ти, опять сложите его цифры.\n");

console.log("Решение:")
var number = 12;
var sNumber = number.toString();
var digits = [];
var sum = 0;

digits = number.toString().split('').map(Number);
console.log("Interpreted digits: " + digits.reverse());

printSumOfDigits();

function printSumOfDigits() {
    sum = sum + digits.reduce(function(a, b) {
        return a + b;
    }, 0);

    if (sum < 9) {
        console.log("Sum is " + sum + ". Calculating sum again");
        printSumOfDigits(number);
    } else {
        console.log("Sum: " + sum);
    }
}