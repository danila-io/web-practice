console.log("Заполните массив 10-ю случайными числами (дробями) от 0 до 1 с помощью цикла. Дроби округляйте до двух знаков в дробной части.");

console.log("Решение:");

var array = [];

for (let index = 0; index < 10; index++) {
    array.push(Math.random().toPrecision(2));
}

console.log("Ответ: " + array);