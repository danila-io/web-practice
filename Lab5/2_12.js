console.log("Дан массив с числами. Найдите среднее арифметическое его элементов (сумма элементов, делить на количество)");

console.log("Решение:");

var array = [11, 20, 10, 5, 3, 7, 88, 3, 4];
var sum = 0;
var avg = 0;

console.log("Изначальный массив: " + array);

array.forEach(e => {
    sum += Math.sqrt(e);
});

console.log("Среднее арифметическое: " + sum / array.length);